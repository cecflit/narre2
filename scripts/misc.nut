//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//return a random integer from <0, max>
function random(max) {
	return rand() % (max + 1);
}

//give Tux a random bonus (50 coins, fireflower, 100 coins, iceflower, egg)
function random_bonus() {
	ran <- ((random(4)) + 1);
	if(ran == 1) {
		Tux.add_coins(50);
		wait(0.05);
		play_sound("sounds/coin.wav");
		wait(0.05);
		play_sound("sounds/coin.wav");
	} else if(ran == 2) {
		Tux.add_bonus("fireflower");
		play_sound("sounds/fire-flower.wav");
	} else if(ran == 3) Tux.add_coins(100);
	else if(ran == 4) {
		Tux.add_bonus("iceflower");
		play_sound("sounds/fire-flower.wav");
	} else if(ran == 5) {
		Tux.add_bonus("grow");
		play_sound("sounds/grow.ogg");
	}
}

//show a "Don't Cheat" notification and kill the player
function kill_cheater() {
	ach_img <- FloatingImage("levels/narre2/images/notifications/ch/nasty.png");
	ach_img.set_anchor_point(ANCHOR_TOP_RIGHT);
	ach_img.set_pos(-20, 35);
	ach_img.set_layer(800);
	ach_img.fade_in(0.5);
	wait(0.05);
	play_sound("levels/narre2/sound/notification.wav");
	wait(2.5);
	Tux.kill(true);
	wait(2.5);
	ach_img.fade_out(0.5);
}

//show a notification (or two) specified by path, no achievements are granted nor dead-locked
function show_notification(path, path2 = false, sound = false) {
	ach_img <- FloatingImage(path);
	ach_img.set_anchor_point(ANCHOR_TOP_RIGHT);
	ach_img.set_pos(-20, 35);
	ach_img.set_layer(800);
	if(path2) {
		img2 <- FloatingImage(path2);
		img2.set_anchor_point(ANCHOR_TOP_RIGHT);
		img2.set_pos(-240, 35);
		img2.set_layer(801);
		img2.fade_in(0.5);
	}
	ach_img.fade_in(0.5);
	wait(0.05);
	if(!sound) play_sound("levels/narre2/sound/achievement.wav");
	else play_sound(sound);
	if(path2) {
		wait(0.05);
		if(!sound) play_sound("levels/narre2/sound/achievement.wav");
		else play_sound(sound);
	}
	wait(5);
	ach_img.fade_out(0.5);
	if(path2) img2.fade_out(0.5);
}
