//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Trigger on-reload events
import("levels/narre2/scripts/reload_handler.nut");

//Secret place
if(state.crystalcave_secret) Secret_Path.fade(1, 0);

//Night
if(state.daytime || state.in_cave) night.fade(1, 1);
else night.fade(0, 1);

//Unlock the world
state.crystalcave <- true;

//Enter or leave the cave section
function go_underground(under) {
	if(under) {
		state.in_cave <- true;
		Over_Decor.fade(0, 1);
		Over_Land.fade(0, 1);
		Outer_Dark.fade(1, 1);
		night.fade(1, 1);
	} else {
		state.in_cave <- false;
		Over_Decor.fade(1, 1);
		Over_Land.fade(1, 1);
		Outer_Dark.fade(0, 1);
		if(!state.daytime) night.fade(0, 1);
	}
}
