//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Reset the need for manual water electrification
state.scelectrify <- false;

//Change daytime and weather every six reloads
if(++state.beforechange > 5) {
	state.beforechange <- 0;
	change_daytime();
	change_weather();
}
