//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Trigger on-reload events
import("levels/narre2/scripts/reload_handler.nut");

//Unlocked worlds
if(state.forest) forest.fade(1, 0);
if(state.jungle) jungle.fade(1, 0);
if(state.underground) underground.fade(1, 0);
if(state.mountains) mountains.fade(1, 0);
if(state.icecave) icecave.fade(1, 0);
if(state.crystalcave) crystalcave.fade(1, 0);
if(state.rocks) rocks.fade(1, 0);
if(state.icecanyon) icecanyon.fade(1, 0);
if(state.swamp) swamp.fade(1, 0);
if(state.desert) desert.fade(1, 0);
if(state.volcano) volcano.fade(1, 0);
if(state.river) river.fade(1, 0);
if(state.stonecave) stonecave.fade(1, 0);
if(state.meadows) meadows.fade(1, 0);
if(state.geysers) geysers.fade(1, 0);
if(state.fortress) fortress.fade(1, 0);
if(state.matrix) matrix.fade(1, 0);
if(state.bonusland) bonus.fade(1, 0);

//Night
if(state.daytime) night.fade(1, 1);
else night.fade(0, 1);

//Reset boats
foreach(wrld in state.worlds) {
	if("sprite-changes" in wrld) {
		foreach(boat in wrld["sprite-changes"]) {
			boat["show-stay-action"] <- true;
		}
	}
}
