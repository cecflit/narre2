//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Trigger on-reload events
import("levels/narre2/scripts/reload_handler.nut");

//Unlocked worlds
if(state.pipeworld) pipe.fade(1, 0);
if(state.industrialworld) industrial.fade(1, 0);
if(state.crystalworld) crystal.fade(1, 0);
if(state.lavacavern) lava.fade(1, 0);

//Night
if(state.daytime) night.fade(1, 1);
else night.fade(0, 1);
