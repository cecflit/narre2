//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Thunderstorm in 'Entering the Tomb'
tombthunder <- false;

//Intro Spawnpoint
if(!("jwintro" in state)) state.jwintro <- false;

//Daytime - 0=day; 1=night
if(!("daytime" in state)) state.daytime <- false;

//Weather - 0=clear; 1=cloudy; 2=snowy/rainy; 3=thunderstorm
if(!("weather" in state)) state.weather <- 0;

//Levels played before daytime/weather change
if(!("beforechange" in state)) state.beforechange <- 0;

//Check whether scripted water electrifying is needed or not (resets in reload_handler.nut)
if(!("scelectrify" in state)) state.scelectrify <- false;

//Check whether Tux is in a cave
if(!("in_cave" in state)) state.in_cave <- false;

//Unlocked dictionary entries
if(!("dictionary_entries" in state)) {
	state.dictionary_entries <- {};
	for(i <- 0; i < 89; ++i) state.dictionary_entries["i" + i.tostring()] <- false;
	state.dictionary_entries["i0"] <- "START";
	state.dictionary_entries["i89"] <- "END";
	for(i <- 1; i < 10; ++i) state.dictionary_entries["i" + i.tostring()] <- true;
	state.dictionary_entries["i48"] <- true;
	state.dictionary_entries["i50"] <- true;
	state.dictionary_entries["i84"] <- true;
}

//Unlocked secret levels
if(!("iceberg_secret" in state)) state.iceberg_secret <- false;
if(!("forest_secret" in state)) state.forest_secret <- false;
if(!("jungle_secret" in state)) state.jungle_secret <- false;
if(!("underground_secret" in state)) state.underground_secret <- false;
if(!("mountains_secret" in state)) state.mountains_secret <- false;
if(!("icecave_secret" in state)) state.icecave_secret <- false;
if(!("crystalcave_secret" in state)) state.crystalcave_secret <- false;
if(!("rocks_secret" in state)) state.rocks_secret <- false;
if(!("icecanyon_secret" in state)) state.icecanyon_secret <- false;
if(!("swamp_secret" in state)) state.swamp_secret <- false;
if(!("desert_secret" in state)) state.desert_secret <- false;
if(!("volcano_secret" in state)) state.volcano_secret <- false;
if(!("river_secret" in state)) state.river_secret <- false;
if(!("stonecave_secret" in state)) state.stonecave_secret <- false;
if(!("meadows_secret" in state)) state.meadows_secret <- false;
if(!("geysers_secret" in state)) state.geysers_secret <- false;
if(!("fortress_secret" in state)) state.fortress_secret <- false;
if(!("matrix_secret" in state)) state.matrix_secret <- false;

//Reached Worlds
if(!("forest" in state)) state.forest <- false;
if(!("jungle" in state)) state.jungle <- false;
if(!("underground" in state)) state.underground <- false;
if(!("mountains" in state)) state.mountains <- false;
if(!("icecave" in state)) state.icecave <- false;
if(!("crystalcave" in state)) state.crystalcave <- false;
if(!("rocks" in state)) state.rocks <- false;
if(!("icecanyon" in state)) state.icecanyon <- false;
if(!("swamp" in state)) state.swamp <- false;
if(!("desert" in state)) state.desert <- false;
if(!("volcano" in state)) state.volcano <- false;
if(!("river" in state)) state.river <- false;
if(!("stonecave" in state)) state.stonecave <- false;
if(!("meadows" in state)) state.meadows <- false;
if(!("geysers" in state)) state.geysers <- false;
if(!("fortress" in state)) state.fortress <- false;
if(!("matrix" in state)) state.matrix <- false;
if(!("bonusland" in state)) state.bonusland <- false;
if(!("pipeworld" in state)) state.pipeworld <- false;
if(!("industrialworld" in state)) state.industrialworld <- false;
if(!("lavacavern" in state)) state.lavacavern <- false;
if(!("crystalworld" in state)) state.crystalworld <- false;

//Intro cutscene play check
if(!("played" in state)) state.played <- false;

//Settings
if(!("voice" in state)) state.voice <- true;
if(!("subtitles" in state)) state.subtitles <- true;
if(!("antiswim" in state)) state.antiswim <- false; //DEPRECATED
if(!("setting_daytime" in state)) state.setting_daytime <- 0; //0 = cycle; 1 = day; 2 = night
if(!("setting_weather" in state)) state.setting_weather <- -1; //-1 = random; 0 = clear; 1 = cloudy; 2 = rain/snow; 3 = thunderstorm
if(!("visual_effects" in state)) state.visual_effects <- 3; //0 = very low; 1 = low; 2 = medium; 3 = high

//Achievements:
//Dead-lockable achievements are integers - 1 is locked, 2 is unlocked, 3 is dead-locked
if(!("achievement_to_find_a_needle_in_a_haystack" in state)) state.achievement_to_find_a_needle_in_a_haystack <- false; //To Find a Needle in a Haystack
if(!("achievement_i_wont_die_anyway" in state)) state.achievement_i_wont_die_anyway <- 1; // I Won't Die Anyway
if(!("achievement_no_second_chances" in state)) state.achievement_no_second_chances <- false; //No Second Chances
if(!("achievement_lazy_engineer" in state)) state.achievement_lazy_engineer <- false; //Lazy Engineer
if(!("achievement_immortality_confirmed" in state)) state.achievement_immortality_confirmed <- false; //Overcharged
if(!("achievement_speeding_fine" in state)) state.achievement_speeding_fine <- false; //Speeding Fine
if(!("achievement_more_sense_than_luck" in state)) state.achievement_more_sense_than_luck <- false; //Encyclopaedist
if(!("achievement_more_luck_than_sense" in state)) state.achievement_more_luck_than_sense <- false; //More Luck than Brains
if(!("achievement_all_roads_lead_to_the_arena" in state)) state.achievement_all_roads_lead_to_the_arena <- false; //All Roads Lead to the Arena
if(!("achievement_penguins_do_fly" in state)) state.achievement_penguins_do_fly <- false; //Penguins Do Fly
if(!("achievement_just_do_it" in state)) state.achievement_just_do_it <- 1; //An Unconvincing Illusion
if(!("achievement_classic_sidescroller" in state)) state.achievement_classic_sidescroller <- false; //Classic Sidescroller
if(!("achievement_mercy" in state)) state.achievement_mercy <- 1; //Mercy
if(!("achievement_grand_traveller" in state)) state.achievement_grand_traveller <- false; //The Grand Traveller
if(!("achievement_cat_eye" in state)) state.achievement_cat_eye <- false; //Cat Eye
if(!("achievement_fish_finger" in state)) state.achievement_fish_finger <- false; //Fish Finger
if(!("achievement_wind_rider" in state)) state.achievement_wind_rider <- false; //Wind Rider
if(!("achievement_toilet_cleaner" in state)) state.achievement_toilet_cleaner <- false; //Toilet Cleaner
if(!("achievement_rabbit_foot" in state)) state.achievement_rabbit_foot <- false; //Rabbit Foot
if(!("achievement_water_bird" in state)) state.achievement_water_bird <- false; //Water Bird
if(!("achievement_perfectionist" in state)) state.achievement_perfectionist <- false; //Perfectionist
if(!("achievement_nested" in state)) state.achievement_nested <- false; //Nested!
if(!("achievement_crossroads" in state)) state.achievement_crossroads <- false; //The Crossroads
if(!("achievement_not_all_heroes_wear_capes" in state)) state.achievement_not_all_heroes_wear_capes <- false; //Not All Heroes Wear Capes
if(!("achievement_set_sail" in state)) state.achievement_set_sail <- false; //Set Sail
if(!("achievement_woody_woods" in state)) state.achievement_woody_woods <- false; //Woody Woods
if(!("achievement_vines_n_swings" in state)) state.achievement_vines_n_swings <- false; //Vines n' Swings
if(!("achievement_humble_hollow" in state)) state.achievement_humble_hollow <- false; //Humble Hollow
if(!("achievement_mountain_madness" in state)) state.achievement_mountain_madness <- false; //Mountain Madness
if(!("achievement_refrigerated" in state)) state.achievement_refrigerated <- false; //Refrigerated
if(!("achievement_tux_the_treasurer" in state)) state.achievement_tux_the_treasurer <- false; //Tux the Treasurer
if(!("achievement_up_and_down" in state)) state.achievement_up_and_down <- false; //Up and Down
if(!("achievement_frosted_heights" in state)) state.achievement_frosted_heights <- false; //Frosted Heights
if(!("achievement_grumpy_ghosts" in state)) state.achievement_grumpy_ghosts <- false; //Grumpy Ghosts
if(!("achievement_dry_dunes" in state)) state.achievement_dry_dunes <- false; //Dry Dunes
if(!("achievement_magma_master" in state)) state.achievement_magma_master <- false; //Magma Master
if(!("achievement_super_splashy" in state)) state.achievement_super_splashy <- false; //Super Splashy
if(!("achievement_disoriented" in state)) state.achievement_disoriented <- false; //Disoriented
if(!("achievement_pollen_bath" in state)) state.achievement_pollen_bath <- false; //Pollen Bath
if(!("achievement_steamy_sauna" in state)) state.achievement_steamy_sauna <- false; //Steamy Sauna
if(!("achievement_befriend_your_foe" in state)) state.achievement_befriend_your_foe <- false; //Befriend Your Foe
if(!("achievement_happy_ending" in state)) state.achievement_happy_ending <- false; //A Happy Ending
if(!("achievement_first_steps" in state)) state.achievement_first_steps <- false; //First Steps

//Secret areas:
if(!("w1_lfrozenhills_s1" in state)) state.w1_lfrozenhills_s1 <- false;
if(!("w1_lfrozenhills_s2" in state)) state.w1_lfrozenhills_s2 <- false;
if(!("w1_ldownfrozencave_s1" in state)) state.w1_ldownfrozencave_s1 <- false;
if(!("w1_ldownfrozencave_s2" in state)) state.w1_ldownfrozencave_s2 <- false;
if(!("w1_ljustanothersnowyday_s1" in state)) state.w1_ljustanothersnowyday_s1 <- false;
if(!("w1_ljustanothersnowyday_s2" in state)) state.w1_ljustanothersnowyday_s2 <- false;
if(!("w1_licypath_s1" in state)) state.w1_licypath_s1 <- false;
if(!("w1_lpipej_s1" in state)) state.w1_lpipej_s1 <- false;
if(!("w1_lplainsice_s1" in state)) state.w1_lplainsice_s1 <- false;
if(!("w1_lmassgla_s1" in state)) state.w1_lmassgla_s1 <- false;
if(!("w1_liceland_s1" in state)) state.w1_liceland_s1 <- false;
if(!("w1_lssland_s1" in state)) state.w1_lssland_s1 <- false;
if(!("w1_lssland_s2" in state)) state.w1_lssland_s2 <- false;
if(!("w2_lwelcomeforest_s1" in state)) state.w2_lwelcomeforest_s1 <- false;
if(!("w2_loverglade_s1" in state)) state.w2_loverglade_s1 <- false;
if(!("w2_lmoisttunnel_s1" in state)) state.w2_lmoisttunnel_s1 <- false;
if(!("w2_lmoisttunnel_s2" in state)) state.w2_lmoisttunnel_s2 <- false;
if(!("w2_lmoisttunnel_s3" in state)) state.w2_lmoisttunnel_s3 <- false;
if(!("w2_lmolehole_s1" in state)) state.w2_lmolehole_s1 <- false;
if(!("w2_lspring_s1" in state)) state.w2_lspring_s1 <- false;
if(!("w2_lrunwoods_s1" in state)) state.w2_lrunwoods_s1 <- false;
if(!("w2_lheavyr_s1" in state)) state.w2_lheavyr_s1 <- false;
if(!("w2_lsumwood_s1" in state)) state.w2_lsumwood_s1 <- false;
if(!("w2_lsumwood_s2" in state)) state.w2_lsumwood_s2 <- false;
if(!("w2_lforshrum_s1" in state)) state.w2_lforshrum_s1 <- false;
if(!("w2_lforshrum_s1" in state)) state.w2_lforshrum_s1 <- false;
if(!("w2_lspikyrifts_s1" in state)) state.w2_lspikyrifts_s1 <- false;
if(!("w2_lspikyrifts_s2" in state)) state.w2_lspikyrifts_s2 <- false;
if(!("w3_lwelcomejungle_s1" in state)) state.w3_lwelcomejungle_s1 <- false;
if(!("w3_lsneakycliff_s1" in state)) state.w3_lsneakycliff_s1 <- false;
if(!("w3_lentertomb_s1" in state)) state.w3_lentertomb_s1 <- false;
if(!("w3_lexplore_s1" in state)) state.w3_lexplore_s1 <- false;
if(!("w3_lgloomy_s1" in state)) state.w3_lgloomy_s1 <- false;
if(!("w3_lthorny_s1" in state)) state.w3_lthorny_s1 <- false;
if(!("w3_llayered_s1" in state)) state.w3_llayered_s1 <- false;
if(!("w4_lhillderground_s1" in state)) state.w4_lhillderground_s1 <- false;
if(!("w4_lgenerictunnel_s1" in state)) state.w4_lgenerictunnel_s1 <- false;
if(!("w4_lwaterfalls_s1" in state)) state.w4_lwaterfalls_s1 <- false;
if(!("w4_lburyruin_s1" in state)) state.w4_lburyruin_s1 <- false;
if(!("w4_ldarktunnel_s1" in state)) state.w4_ldarktunnel_s1 <- false;
if(!("w4_lll_s1" in state)) state.w4_lll_s1 <- false;
if(!("w4_labandonlair_s1" in state)) state.w4_labandonlair_s1 <- false;
if(!("w5_lwelcomemountains_s1" in state)) state.w5_lwelcomemountains_s1 <- false;
if(!("w5_lreallyhigh_s1" in state)) state.w5_lreallyhigh_s1 <- false;
if(!("w5_ldontvalley_s1" in state)) state.w5_ldontvalley_s1 <- false;
if(!("w5_lsnowypeaks_s1" in state)) state.w5_lsnowypeaks_s1 <- false;
if(!("w5_lconquer_s1" in state)) state.w5_lconquer_s1 <- false;
if(!("w5_lmntbreeze_s1" in state)) state.w5_lmntbreeze_s1 <- false;
if(!("w6_lfrozendepth_s1" in state)) state.w6_lfrozendepth_s1 <- false;
if(!("w6_lfrozendepth_s2" in state)) state.w6_lfrozendepth_s2 <- false;
if(!("w6_lspikes_s1" in state)) state.w6_lspikes_s1 <- false;
if(!("w6_lfreezingcave_s1" in state)) state.w6_lfreezingcave_s1 <- false;
if(!("w6_lgetslippery_s1" in state)) state.w6_lgetslippery_s1 <- false;
if(!("w6_ldeepcold_s1" in state)) state.w6_ldeepcold_s1 <- false;
if(!("w6_ldeepplaces_s1" in state)) state.w6_ldeepplaces_s1 <- false;
if(!("w6_ldeepplaces_s2" in state)) state.w6_ldeepplaces_s2 <- false;
if(!("w7_lwelcomecrystal_s1" in state)) state.w7_lwelcomecrystal_s1 <- false;
if(!("w7_ldiamondssector_s1" in state)) state.w7_ldiamondssector_s1 <- false;
if(!("w7_lrubyred_s1" in state)) state.w7_lrubyred_s1 <- false;
if(!("w7_lpurplecave_s1" in state)) state.w7_lpurplecave_s1 <- false;
if(!("w7_lcrcl_s1" in state)) state.w7_lcrcl_s1 <- false;
if(!("w8_llakevalley_s1" in state)) state.w8_llakevalley_s1 <- false;
if(!("w8_lexplorelake_s1" in state)) state.w8_lexplorelake_s1 <- false;
if(!("w8_lundercave_s1" in state)) state.w8_lundercave_s1 <- false;
if(!("w8_lrocksolid_s1" in state)) state.w8_lrocksolid_s1 <- false;
if(!("w8_lrocksolid_s2" in state)) state.w8_lrocksolid_s2 <- false;
if(!("w8_lnoveg_s1" in state)) state.w8_lnoveg_s1 <- false;
if(!("w9_ltreeguards_s1" in state)) state.w9_ltreeguards_s1 <- false;
if(!("w9_lfairfrost_s1" in state)) state.w9_lfairfrost_s1 <- false;
if(!("w9_lisleice_s1" in state)) state.w9_lisleice_s1 <- false;
if(!("w9_lfridgy_s1" in state)) state.w9_lfridgy_s1 <- false;
if(!("w9_lbitter_s1" in state)) state.w9_lbitter_s1 <- false;
if(!("w10_lmuddylakes_s1" in state)) state.w10_lmuddylakes_s1 <- false;
if(!("w10_lmuddylakes_s2" in state)) state.w10_lmuddylakes_s2 <- false;
if(!("w10_lshowme_s1" in state)) state.w10_lshowme_s1 <- false;
if(!("w10_lmuddive_s1" in state)) state.w10_lmuddive_s1 <- false;
if(!("w10_loverwisp_s1" in state)) state.w10_loverwisp_s1 <- false;
if(!("w10_lmuddyswamp_s1" in state)) state.w10_lmuddyswamp_s1 <- false;
if(!("w10_lghostspot_s1" in state)) state.w10_lghostspot_s1 <- false;
if(!("w11_lwelcomedesert_s1" in state)) state.w11_lwelcomedesert_s1 <- false;
if(!("w11_lsandcave_s1" in state)) state.w11_lsandcave_s1 <- false;
if(!("w11_lnoshade_s1" in state)) state.w11_lnoshade_s1 <- false;
if(!("w11_lquickpuddles_s1" in state)) state.w11_lquickpuddles_s1 <- false;
if(!("w11_lsmalloasis_s1" in state)) state.w11_lsmalloasis_s1 <- false;
if(!("w11_lcaczone_s1" in state)) state.w11_lcaczone_s1 <- false;
if(!("w11_ldrought_s1" in state)) state.w11_ldrought_s1 <- false;
if(!("w12_lwelcomevolcano_s1" in state)) state.w12_lwelcomevolcano_s1 <- false;
if(!("w12_lerupt_s1" in state)) state.w12_lerupt_s1 <- false;
if(!("w12_lburnbridge_s1" in state)) state.w12_lburnbridge_s1 <- false;
if(!("w12_lburntplat_s1" in state)) state.w12_lburntplat_s1 <- false;
if(!("w12_lllakes_s1" in state)) state.w12_lllakes_s1 <- false;
if(!("w12_lllakes_s2" in state)) state.w12_lllakes_s2 <- false;
if(!("w12_lwarmcore_s1" in state)) state.w12_lwarmcore_s1 <- false;
if(!("w12_ldefheat_s1" in state)) state.w12_ldefheat_s1 <- false;
if(!("w13_lwelcomeriver_s1" in state)) state.w13_lwelcomeriver_s1 <- false;
if(!("w13_lboatwrld_s1" in state)) state.w13_lboatwrld_s1 <- false;
if(!("w13_lweird_s1" in state)) state.w13_lweird_s1 <- false;
if(!("w13_lspiky_s1" in state)) state.w13_lspiky_s1 <- false;
if(!("w13_lyetbridge_s1" in state)) state.w13_lyetbridge_s1 <- false;
if(!("w13_ltinyisle_s1" in state)) state.w13_ltinyisle_s1 <- false;
if(!("w13_lwetwater_s1" in state)) state.w13_lwetwater_s1 <- false;
if(!("w13_lwetwater_s2" in state)) state.w13_lwetwater_s2 <- false;
if(!("w13_lcalmflow_s1" in state)) state.w13_lcalmflow_s1 <- false;
if(!("w14_lwelcomenrwcave_s1" in state)) state.w14_lwelcomenrwcave_s1 <- false;
if(!("w14_lceilcoll_s1" in state)) state.w14_lceilcoll_s1 <- false;
if(!("w14_lzigzag_s1" in state)) state.w14_lzigzag_s1 <- false;
if(!("w14_lzigzag_s2" in state)) state.w14_lzigzag_s2 <- false;
if(!("w14_lzigzag_s3" in state)) state.w14_lzigzag_s3 <- false;
if(!("w14_lzigzag_s4" in state)) state.w14_lzigzag_s4 <- false;
if(!("w14_lladderjump_s1" in state)) state.w14_lladderjump_s1 <- false;
if(!("w14_lrockcrush_s1" in state)) state.w14_lrockcrush_s1 <- false;
if(!("w14_lstonezone_s1" in state)) state.w14_lstonezone_s1 <- false;
if(!("w14_ldim_s1" in state)) state.w14_ldim_s1 <- false;
if(!("w14_lladtramp_s1" in state)) state.w14_lladtramp_s1 <- false;
if(!("w15_lladderspike_s1" in state)) state.w15_lladderspike_s1 <- false;
if(!("w15_lplainflowers_s1" in state)) state.w15_lplainflowers_s1 <- false;
if(!("w15_lstripy_s1" in state)) state.w15_lstripy_s1 <- false;
if(!("w15_lgrplateau_s1" in state)) state.w15_lgrplateau_s1 <- false;
if(!("w15_lburningway_s1" in state)) state.w15_lburningway_s1 <- false;
if(!("w15_llandhay_s1" in state)) state.w15_llandhay_s1 <- false;
if(!("w15_lcloverflds_s1" in state)) state.w15_lcloverflds_s1 <- false;
if(!("w15_lwavy_s1" in state)) state.w15_lwavy_s1 <- false;
if(!("w16_lwelcomegeysers_s1" in state)) state.w16_lwelcomegeysers_s1 <- false;
if(!("w16_lhotspring_s1" in state)) state.w16_lhotspring_s1 <- false;
if(!("w16_llandhot_s1" in state)) state.w16_llandhot_s1 <- false;
if(!("w16_lhothumid_s1" in state)) state.w16_lhothumid_s1 <- false;
if(!("w17_lwelcomefort_s1" in state)) state.w17_lwelcomefort_s1 <- false;
if(!("w17_lspacedng_s1" in state)) state.w17_lspacedng_s1 <- false;
if(!("w17_lspacedng_s2" in state)) state.w17_lspacedng_s2 <- false;
if(!("w17_lflood_s1" in state)) state.w17_lflood_s1 <- false;
if(!("w17_llavapol_s1" in state)) state.w17_llavapol_s1 <- false;
if(!("w17_lmovings_s1" in state)) state.w17_lmovings_s1 <- false;
if(!("w17_lexplodunge_s1" in state)) state.w17_lexplodunge_s1 <- false;
if(!("w17_ltraps_s1" in state)) state.w17_ltraps_s1 <- false;
if(!("w17_lcomplex_s1" in state)) state.w17_lcomplex_s1 <- false;
if(!("w17_lcomplex_s2" in state)) state.w17_lcomplex_s2 <- false;
if(!("w17_lcomplex_s3" in state)) state.w17_lcomplex_s3 <- false;
if(!("w17_lcomplex_s4" in state)) state.w17_lcomplex_s4 <- false;
if(!("w18_lwelcomematr_s1" in state)) state.w18_lwelcomematr_s1 <- false;
if(!("w18_llasertraps_s1" in state)) state.w18_llasertraps_s1 <- false;
if(!("w18_llifts_s1" in state)) state.w18_llifts_s1 <- false;
if(!("w18_lboxbeam_s1" in state)) state.w18_lboxbeam_s1 <- false;
if(!("w18_lpushers_s1" in state)) state.w18_lpushers_s1 <- false;
if(!("w18_llaserrun_s1" in state)) state.w18_llaserrun_s1 <- false;
if(!("w18_ldiginsane_s1" in state)) state.w18_ldiginsane_s1 <- false;
if(!("w18_lvirmad_s1" in state)) state.w18_lvirmad_s1 <- false;
if(!("w19_lfork_s1" in state)) state.w19_lfork_s1 <- false;
