//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

cover_front <- FloatingImage("levels/narre2/images/dictionary/dictionary_front.png");
cover_back <- FloatingImage("levels/narre2/images/dictionary/dictionary_back.png");
dict <- FloatingImage("levels/narre2/images/dictionary/dictionary_open.png");
illustration <- FloatingImage("images/creatures/nolok/jump-1.png");
cover_front.set_layer(101);
cover_back.set_layer(101);
dict.set_layer(101);
current_page <- 0;
titles <- [
"START"
_("Tux")
_("Penny")
_("Neighbours")
_("Pex")
_("Treasury")
_("Achievements")
_("Settings")
_("Checkpoint")
_("Freezing Iceberg")
_("Green Forest")
_("Mysterious Jungle")
_("Mouldy Underground")
_("Windy Mountains")
_("Chilly Cave")
_("Cave of Crystals")
_("Vertiginous Rocks")
_("Canyon of Frost")
_("Swamp of Ghosts")
_("Desert of Dryness")
_("Volcano of Death")
_("Wild River")
_("Rocky Cave")
_("Flowering Meadows")
_("Sulphuric Geysers")
_("Sinister Fortress")
_("Dead Isle")
_("Bonusland")
_("Pipe World")
_("Crystal World")
_("Industrial World")
_("Lava Cavern")
_("Angry Puddle")
_("Totem")
_("Tumbleweed")
_("Dödleytree")
_("Yeti")
_("Krosh")
_("Crystallon")
_("Rock Eater")
_("Iciclion")
_("Ghost Tree")
_("Zigureth")
_("Magma Cube")
_("Fishmaster")
_("Rockrosh")
_("Angry Fairy")
_("Spitter")
_("Nolok")
_("Nolok")
_("Matrix")
_("Matrix")
_("Quest 1")
_("Quest 2")
_("Quest 3")
_("Quest 4")
_("Quest 5")
_("Quest 6")
_("Quest 7")
_("Quest 8")
_("Quest 9")
_("Quest 10")
_("Quest 11")
_("Quest 12")
_("Quest 13")
_("Quest 14")
_("Quest 15")
_("Quest 16")
_("Quest 17")
_("Quest 18")
_("Quest 19")
_("Quest 20")
_("Quest 21")
_("Quest 22")
_("Quest 23")
_("Quest 24")
_("Quest 25")
_("Quest 26")
_("Quest 27")
_("Quest 28")
_("The North Coast Prison")
_("The Lone Pine")
_("Quest 31")
_("Quest 32")
_("Enemy")
_("Mud")
_("Quicksand")
_("Bouncy Pad")
_("Pusher")
"END"];

descriptions <- [
"START"
_("Tux is the hero of this story. He is brave and determined, and never gives up. He has a girlfriend, Penny, for whom he would do absolutely anything and everything. His arch enemy is Nolok, a crocodile who is usually plotting a plan to get Penny from Tux just for his own entertainment. Tux's favourite food are herrings and his favourite pasttime is being lazy. When necessity calls for it though, his laziness all but disappears.")
_("Penny is a thin and purple-ish penguin. Her life is mostly a circular line of events that is composed exclusively of being kidnapped by Nolok and being rescued by Tux, her boyfriend. She is extremely loyal to Tux and loves to spend as much time with him as possible.")
_("Rucks and Mallie are Tux and Penny's neighbours and good friends. They often share parties, dine togenther and spend quite some time at each other's iggloos. They share their love for herrings.")
_("Pex is a bit of a local weirdo in Tux's village. He usually smells of rotten fish and as he gets older, he often comes up with nonsense stories and believes them. The local community often refer to him as \"The Tinfoilhatter\". Despite him going a bit bananas, he is still regarded as mostly wise and knowledgable. When there are any problems in the community, Pex is usually the go-to penguin.")
_("The treasury is an ironic room in the Intro level. Its sole purpose is to keep one singular coin with no special properties away from Tux. If, however, Tux turns out to be serious about clearing every single possible coin, enemy and secret area in the worldmap and the one last pesky coin is the only thing keeping him from completing this task, the treasury will, eventually, let Tux get to its contents.")
_("Achievements are virtual trophies that Tux gets for completing side quests, making his journey more difficult, finding secrets and behaving certain way in certain levels. They don't serve much purpose outside of making Tux feel good about his accomplishments. Some achievements are easy to get, some are more difficult, some inevitable and some can even be permanently dead-locked if Tux isn't careful, resulting in him never getting a second chance at them.")
_("Settings in this add-on allow the player to adjust several aspect of gameplay. Via the settings, it is possible to set a certain time of day and specific weather (by default, they will cycle), turn subtitles and voice narration on and off and even disable particles and cetrain effect to enhance playability on slower computers. The last option was originally supposed to be more powerful, but Narre was too lazy to implement it properly, so it only has a binary Disabled/Enabled state and nothing more.")
_("Checkpoints allow Tux to magically respawn in certain locations if he were to die. However, activating a checkpoint before getting the \"I Won't Die Anyway\" achievement dead-locks it permanently, so Tux might not want to use their power...")
_("Freezing Iceberg is a medium sized glacier which is a part of Icy Island. It is home to Tux and Penny and it's also the first world on Tux's adventure to save Penny and the world from the vicious Matrix. There is a castle on the Freezing Iceberg that is controlled by an unknown underwater creature that lives inside an ice cold puddle in the castle's throne room. Since noone knows what the creature is, it is commonly referred to simply as the Angry Puddle.")
_("Green Forest is the second world on Tux's adventure to the Matrix. It is a medium sized island with lukewarm temperatures, ruled by an evil emperor known as the Totem.")
_("Mysterious Jungle is the third world on Tux's adventure. It is a hot and humid island overgrown with dense greenery, which includes almost impenetrable trees, pogonias and thorny bushes. There are signs of an ancient civilisation on the island, including remains of old statues and rotting wooden bridges. There is also a large tomb in the south of the jungle, where the civilisation presumably buried their dead. The jungle is ruled by Tumbleweed, an adventurer turned dictator who is determined to let nobody pass the jungle to get to the Matrix.")
_("Mouldy Underground is the fourth world on Tux's adventure. It is a small island overgrown with grass and shrubbery so thick that it is virtually impossible to pass through. Luckily, however, there is an extensive network of underground tunnels that are used for traversing the land exclusively. There is no permenent population there, but some travellers are known to have settled there for prolonged periods of time, leaving abandoned shelters and even lairs. There is a tree growing at the east coast of the island which is known to ocasionally hunt travellers in the underground using its enormous roots. It is known as the Dödleytree, named after Dödley, the first traveller who got killed by it while attempting to build a safe passage for future passers by.")
_("Windy Mountains is the fifth world on Tux's adventure. They are a mountain range spanning an entrire cold, uninhabited island. Travellers are known to avoid this island if possible, as the terrain is very difficult to navigate and there are even rumours of a stupid, violent and evil Yeti hunting on the island.")
_("Chilly Cave is the sixth world on Tux's adventure. It is a medium sized island with a very rough surface so most, if not all, travellers choose to traverse it by the cave complex that spans the entire island. These caves are extensive, cold and dangerous, but, unlike the surface, they are mapped and ocasionally travelled. There is a frozen palace at the northern cave entrance, which is known to be inhabited by Krosh, an overgrown icecrusher who tends to squish passers by. Travellers usually wait for him to go to sleep before attempting to pass through the palace. Tux, however, will not have time to wait an can only hope that Krosh won't bother him much.")
_("Cave of Crystals is the seventh world on Tux's adventure. It is a cold island with no paths and snow-covered abysses, making it extremely dangerous to traverse. Therefore, just like the Chilly Cave, travellers use the island's cave system to navigate. The caves on this island are filled with many different types of rare gems and crystals, making it quite popular for treasure seekers. All the treasure in these caves is natural, consisting of diamonds, emeralds, rubies, amethysts, citrines, topazes and translucent gems, which are made mostly of quartz. There is an underground crystal fortress on the north side of the island, where a vicious monster called Crystallon resides.")
_("Vertiginous Rocks is the eighth world on Tux's adventure. It is a large and rocky island with very little fauna and flora. The rocks on this island tend to form extremely tall and steep formations, making the island difficult to navigate. There are no caves to help travellers get by, but some choose to swim through a mountain lake in the middle of the island to avoid some of the most difficult terrain. There is a bunker at one of the shores where Rock Eater lives. The Rock Eater is an anomaly in evolution - an animal whose skin is mostly made of rock who often hunts around its bunker for food. Since there is very little food on the island, the Rock Eater mainly lives off travellers who pass by as well as some softer types of stone.")
_("Canyon of Frost is the ninth world on Tux's adventure. It is a freezing island with a gigantic canyon in the middle. Since there's no way to navigate around or at the bottom of the canyon, travellers have to navigate the ice sculptures growing out of the abyss. The island is ruled by a self-proclaimed protector called Iciclion. Iciclion is not willing to let anyone past his tower and has killed numerous adventurers over the years.")
_("Swamp of Ghosts is the tenth world on Tux's adventure. It is a relatively big and boggy island which is usually avoided by travellers if possible, because it is said to be cursed and full of dark magic. There is a part of the island where there's always night and also a cave which is said to eat travellers alive. There is a tall tower on the coast which is said to be the home of Ghost Tree, a sentient tree posessed by evil spirits.")
_("Desert of Dryness os the eleventh world on Tux's adventure. It is a relatively big, dry island of sand where it never rains. It is inhabited by undead skulls and skelehands. The only vegetation on this island are cacti. Travellers usually regard this island as relatively easy to navigate. Apart from some quicksand and lack of water, the terrain is quite easy. However, outside of one single cave, there is no shade on the island so it is not recommended to stay there for too long. The island is ruled by Zigureth, an ancient creature who likes deadly games, but is known for playing them fair.")
_("Volcano of Death is the twelfth world on Tux's adventure. It is a volcanic island with numerous active volcanoes, lava springs and magma plains. There are some paths there that are regarded as generally safe but travellers have to pass through several dodgy bits if they want to cross the island. The island is ruled by a creature known as the Magma Cube, who lives in a fortified castle on the southern coast.")
_("Wild River is the thirteenth world on Tux's adventure. It is a massive landmass inhabited by a lot of venomous snakes, dangerous animals and even deadly plants. For that reason, the land is never travelled. Thankfully, there is an extensive network of rivers on this island, which provide a much safer passage throughout the land. For that reason, piers and bridges were built in the past. Most of them have rotten and desintegrated since, but the water is still the way to traverse this world. The greatest danger comes from a flooded mansion in the eastern delta, where a crazy overgrown fish known as the Fishmaster resides.")
_("Rocky Cave is the fourteenth world on Tux's adventure. It is a medium sized island with no vegetation or wildlife. Because of its impassible terrain, which is ten times rougher than that of the Vertiginous Rocks, the island is always traversed by its system of caves that span from the western coast to the eastern one. The eastern entrance, however, is guarded by Rockrosh, Krosh's rocky cousin.")
_("Flowering Meadows is the fifteenth world on Tux's adventure. It is a relatively large and flat island with no trees, just grass and flowers. There is a network of paths on the island, built by the native population. This population, however, was, due to its proximity to the Matrix, all captured by it, leaving only a few haybales behind. In the absence of the natives, an evil fairy has taken the island over, ruling from a palace near the eastern coast.")
_("Sulphuric Geysers is the sixteenth world on Tux's adventure. It is a large island covered by sulphur and springs of sulphuric water. Apart from some fish, some jumpies and quite a few explosive inhabitants, there is no permanent population there. The island is quite hot and extremely humid. There is a chateau in the north inhabited by Spitter, a vicious creature who lives in boiling sulphuric water and tries to kill passers by.")
_("Sinister Fortress is the seventeenth world on Tux's adventure. It is a massive fortified castle built by Nolok's ancestors and filled with spike traps and Nolok's minions. It is the nearest landmass to the Matrix, which is quite suspicious to Tux but it also means that he will have to go through it. Since it is the largest of Nolok's castles, Tux is afraid he might meet him there.")
_("Dead Isle is a tiny island in the middle of nowhere in the deep ocean. The closest structure is Nolok's Sinister Fortress and the closest natural landmass are the Sulphuric Geysers. The remoteness of this island is likely the reason why the Matrix was built here, out of reach for anyone. The Matrix itself is built in the ocean and the original isle only serves as a dock for it. The Matrix is the eighteenth and final world on Tux's adventure.")
_("Bonusland is a set of five islands in an undisclosed location. Tux can access these island after defeating the Matrix. There is a fork on the centre island that unlocks the four corner islands. Each of these corner islands contains five bonus levels. In these bonus levels, there are some challenges for which Tux can get a few achievements.")
_("Pipe World is one of the Bonusland islands. It is a flat grassy island filled with pipes. The entire island is traversible by them, on ground, underground and even in the sky.")
_("Crystal World is one of the Bonusland islands. It is a crystal-filled island with no caves and a lot of rare and expensive gems.")
_("Industrial World is one of the Bonusland islands. It is a rocky island filled with metal constructions, nets and spiders.")
_("Lava Cavern is one of the four Bonusland islands. It is a hot volcanic island with a cave full of mysteries.")
_("Angry Puddle is a nickname for an unknown monster who lives in an ice cold puddle in the throne room of the castle on Freezing Iceberg. The monster controls the trapped exit door of the castle and likes to throw bouncing snowballs at anyone who gets anywhere near it. Many travellers tried and failed to kill the Angry Puddle by filling the puddle with solid matter, poisoning it or blocking it from any air intake, but the monster always won.")
_("Totem is a vicious wooden monster and the emperor of the Green Forest. He is a multiform composed of several stumps who share the came consciousness. He lives in a fortress on the eastern coast, where he troubles passing travellers. Some have managed to kill some of the stumps, but as long as at least one survives, the Totem is eventually able to regenerate the rest.")
_("Tumbleweed is originally an adventurer from a distant savannah. He decided to defeat the Matrix just like Tux, but when he reached the Mysterious Jungle, he started to think that the Matrix only kidnaps because it feels threatened, so he settled in the only tower in the jungle and decided to stop anyone trying to go to the Matrix, thinking that doing so would stop the disappearances.")
_("Dödleytree is a sentient tree growing above the Mouldy Underground. It often hunts for animals and even travellers using its roots. Dödleytree is not inherently evil, it hunts exclusively for nutrients. It is not the only one of its kind on the island, but it is the only one whose roots reach to the underground tunnels. Some travellers have tried to prune the roots or even kill the tree, but none succeeded. The tree is named after Dödley, the first documented victim.")
_("Yeti is a stupid and violent bear-like creature who is rumoured to hunt in the Windy Mountains. There are no photos or video evidence, but a number of travellers who passed through the mountains swear it is real. Most of them provided the same description and claim that the Yeti lives in an arena-like hollow in one of the mountains which is said to have been created in the ancient times by ancient gods, and the Yeti is said to be its protector.")
_("Krosh is an overgrown icecrusher who lives near the north entrance to the Chilly Cave. Krosh is much more intelligent than most icecrushers, and also a lot more violent. He tries to squish all travellers who pass by. Most pass by him when he sleeps, but that can often be a very long wait since icecrushers only sleep for a few hours per month. A group of travellers have managed to install spikes underneath Krosh's resting position, but the spikes froze over, making them ineffective.")
_("Crystallon is a crystallic lifeform who lives in the Cave of Crystals. He is known for shooting sharp crystals at bycomers. Travellers usually wear helmets when traversing the Cave of Crystals, so they don't need to fear it, however, it has been reported that the monster has been getting more and more violent lately, shooting much chunkier crystals and even threatening travellers with collapsing the cave.")
_("Rock Eater is a rock-based lifeform living in the Vertiginous Rocks. He uses heavy rocks to squish travellers who came by his castle. It is theorised that Rock Eater can be defeated with either hot lava or strong acid, but nobody has yet attemped to put their luck to the test.")
_("Iciclion is a vicious, ice-based lifeform living in a tower in Snow Valley in the Canyon of Frost. Iciclion is known for shooting icicles at travellers. It has originally been thought that Iciclion hunts for food, but closer observations concluded that he kills mainly for entertainment.")
_("Ghost Tree is a sentient tree, supposedly posessed by evil spirits, which grows in a cursed temple on the southern coast of the Swamp of Ghosts. The tree attacks travellers using its roots and breathes three different types of ghostly lights on a cycle. It is theorised that forcefully disrupting this cycle would instantly kill the tree, but nobody was yet brave enough to try. Most travellers avoid the entire island.")
_("Zigureth is an ancient being who lives in a pyramid in the Desert of Dryness. He always plays deadly games with travellers, and is known for playing them fair. However, unlike any travellers, Zigureth is immortal and when he loses, he is usually resurrected within a month.")
_("Magma Cube is a being of pure magma living in the fortress in the Volcano of Death. It is known for burning travellers to a crisp, but it is hurt by water. Nobody has ever managed to extinguish it to death, but a few came close.")
_("Fishmaster is an overgrown evil fish who lives in a sunken palace in the eastern delta of the Wild River. He is not strong, but he is good at using weapons, especially tridents. Whenever a traveller goes by to get past the palace, the Fishmaster tries to kill them. For that reason, most travellers avoid the eastern delta if they can.")
_("Rockrosh is an overgrown Stone Crusher, a cousin of Krosh, who tends to squish bycomers to pancakes. He lives in a maze-like subsystem of caves in the Rocky Cave. Like Krosh, he only sleeps for a few hours every month, meaning that Tux will likely have to face him awake.")
_("Angry Fairy is an evil witch who took the opportunity after the native population of Flowering Meadows was all kidnapped by the Matrix and took the island over. She has a magic wand she uses to harm and even kill travellers who attempt to cross the island. She lives in a fairytale palace on the eastern coast.")
_("Spitter is a vicious creature made of sulphuric rock and sulphuric acid. He lives in a small chateau near the northern coast of the Sulphuric Geysers. He attacks travellers by spitting sulphuric acid at them.")
_("Nolok is Tux's arch enemy. He controls a sizeable army of minions known as badguys. His favourite entertainment is kidnapping Penny, Tux's girlfriend. Being a crocodile, he has no use for her, but he enjoys watching Tux struggle to save her. He owns a large number of castles and fortresses all around the Earth. His largest fortresses are the one at Nolok's Snowy Mountain near the Tropical Island and the Sinister Fortress near the Sulphuric Geysers.")
_("Nolok is a crocodile from an evil family. He was brought up by his twisted father who taught him his ways of evil. Nolok spent most of his life as an evil maniac, manipulating others, kidnapping Penny and laughing at others' struggles and misfortunes. He only began to realise how wrong his actions were after his father died and uploaded his consciousness to the Matrix, as he ordered all badguys to protect the Matrix at all costs, making them not obey Nolok, essentially trapping him in one room. After Tux freed him on his way to the Matrix, Nolok acknowledged the implications of his past behaviour and swore to change his ways and to help Tux defeat the Matrix.")
_("The Matrix is a powerful computer of unknown origin. The Matrix's programming is so complex that it is thought to be sentient. For unknown reasons, the Matrix has decided to upload every sentient mind on the face of the planet to its virtual world and suspend their physical forms. This could eventually lead to everyone living in a virtual reality. After the Matrix kidnapped Tux's girlfriend, Penny, he decided to tackle the machine for himself. The Matrix is located suspiciously close to Sinister Fortress, Nolok's largest castle.")
_("The Matrix is a powerful computer built by Nolok's father to save his mind from his dying body. The Matrix's programming is so complex that it its thought to be sentient. Because of Nolok's father's opinion that if he can't live outside of a simulation then noone can, the Matrix wants to upload every sentient mind on the face of the planet to its virtual world and suspend their physical being. This could eventually lead to everyone living in a virtual reality. After the Matrix kidnapped Tux's girlfriend, Penny, he decided to tackle the machine for himself. The Matrix is located close to Sinister Fortress, a massive castle Nolok inherited from his father.")
_("Quest 1")
_("Quest 2")
_("Quest 3")
_("Quest 4")
_("Quest 5")
_("Quest 6")
_("Quest 7")
_("Quest 8")
_("Quest 9")
_("Quest 10")
_("Quest 11")
_("Quest 12")
_("Quest 13")
_("Quest 14")
_("Quest 15")
_("Quest 16")
_("Quest 17")
_("Quest 18")
_("Quest 19")
_("Quest 20")
_("Quest 21")
_("Quest 22")
_("Quest 23")
_("Quest 24")
_("Quest 25")
_("Quest 26")
_("Quest 27")
_("Quest 28")
_("The North Coast Prison is a prison originally built by the natives of Flowering Meadows to deal with intruders. However, the island has been peaceful for eons now and the prison is normally never used. After the native population of the island was kidnapped though, the self-proclaimed boss of the island, the Angry Fairy, has restored it and plans to lock up any potential new inhabitants of the island (and even the original ones if the Matrix were to be defeated). For that reason, Tux is tasked by one of the only remainig natives on the island to destroy the prison to prevent the evil witch from using it.")
_("The Lone Pine is the only tree in the Flowering Meadows, and it is considered sacred by the native population. However, after they all disappeared to the Matrix, a vandal from a nearby island travelled to the Flowering Meadows to chop the tree down for fun, as he sniffed the opportunity. Tux was tasked by one of the the only remaining natives, who hid from the Matrix in a Faraday cage, to save the sacred pine from the intruder.")
_("Quest 31")
_("Quest 32")
_("Enemies try to stop Tux on his journey. Tux's biggest enemies are bosses who control individual islands, but enemies far more common, living on every bit of land and water, are the badguys. These minions are and have been genetically engineered and controlled by Nolok's family for many decades. Some of them Tux has seen before but some are relatively new additions to Nolok's portfolio. These include Tree Guardians, Flame Fish, Bees and more.")
_("Mud is a barely liquid, boggy and very sticky substance common in the Swamp of Ghosts. Mudpools are generally deep and it's esentially impossible to escape them once stepped in. For that reason, these mudpools are incredibly dangerous as anyone stepping in them will slowly sink and suffocate, with no way of escaping.")
_("Quicksand is a variation of sand that is so fine that it esentially flows. This makes it impossible to escape from it once stepped in. Since quicksand patches tend to be quite deep, when they are stepped in, the unfortunate victim can only wait to slowly sink in and suffocate, with no way of escaping. Quicksand can be found in the Desert of Dryness. It is home to an undead creature known as the Skelehand.")
_("Bouncy Pad is a type of rock formation that is relatively common in the Sulphuric Geysers. It is called so because there is a constant flow of steam underneath it, bursting all out at the same time every few seconds, which can be used to propel one up by a few metres.")
_("Pusher is a type of circuit in the Matrix that has strong repulsors installed around it to prevent potential uninvited guests from tampering with their insides. This can be exploited to purposely launch oneself to any direction.")
"END"];

illustrations <- [
"START"
"images/creatures/tux/big/buttjump-1.png"
"images/creatures/penny/penny.png"
"levels/narre2/images/creatures/neighbours/neighbours.png"
"levels/narre2/images/creatures/pex/pex.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"images/objects/resetpoints/bell-m.png"
"images/tiles/snow/background.png"
"images/tiles/forest/foresttiles-9.png"
"images/tiles/forest/statue2.png"
"images/tiles/forest/underground/background1.png"
"images/tiles/snowmountain/ground2.png"
"images/tiles/darksnow/convex.png"
"levels/narre2/images/decorations/topaz_right.png"
"images/tiles/doodads/stone1.png"
"images/tiles/snow/air.png"
"images/particles/ghost1.png"
"levels/narre2/images/tiles/quicksand/quicksand_top_1.png"
"levels/narre2/images/tiles/fire4.png"
"images/tiles/water/antarctic-1.png"
"levels/narre2/images/tiles/stone-concave.png"
"levels/narre2/images/tiles/flowers.png"
"levels/narre2/images/tiles/geyser-still.png"
"images/tiles/castle/stonewindow.png"
"levels/narre2/images/tiles/matrcolour.png"
"levels/narre2/images/decorations/billboard-bonus.png"
"levels/narre2/images/tiles/pipe/red.png"
"images/tiles/crystalcave/background.png"
"images/tiles/blocks/industrial.png"
"levels/narre2/images/tiles/lava-convex.png"
"levels/narre2/images/placeholders/placeholder.png"
"images/creatures/totem/stacked.png"
"images/creatures/tumbleweed/tumbleweed0.png"
"levels/narre2/images/placeholders/placeholder.png"
"images/creatures/yeti/y-jump.png"
"images/creatures/icecrusher/krosh.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"images/creatures/ghosttree/worldmap_1.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"images/creatures/nolok/jump-1.png"
"images/creatures/nolok/jump-1.png"
"levels/narre2/images/tiles/trace/blue.png"
"levels/narre2/images/tiles/trace/blue.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"images/tiles/forest/foresttiles-9.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/creatures/tree_guardian/left-0.png"
"levels/narre2/images/tiles/mud/mud_top_0.png"
"levels/narre2/images/tiles/quicksand/quicksand_top_1.png"
"levels/narre2/images/placeholders/placeholder.png"
"levels/narre2/images/creatures/matrixguys/yellowguy.png"
"END"];

function load_dictionary() {
	cover_front.set_visible(true);
	sector.Text.set_text("");
	sector.Text.set_anchor_point(ANCHOR_MIDDLE);
	sector.Text.set_pos(-287, -250);
	sector.Text.set_font("big");
	sector.Text.set_back_fill_color(0, 0, 0, 0);
	sector.Text.set_front_fill_color(0, 0, 0, 0);
	sector.Text.set_text_color(1, 1, 0, 1);
	sector.Text.set_visible(false);
	sector.TextArray.add_text("");
	sector.TextArray.set_anchor_point(ANCHOR_MIDDLE);
	sector.TextArray.set_pos(247, -50);
	sector.TextArray.set_back_fill_color(0, 0, 0, 0);
	sector.TextArray.set_front_fill_color(0, 0, 0, 0);
	sector.TextArray.set_visible(false);
}

function next_page() {
	if(state.dictionary_entries["i" + current_page.tostring()] == "END") return;
	if(!current_page) {
		cover_front.set_visible(false);
		dict.set_visible(true);
		sector.Text.set_visible(true);
		sector.TextArray.set_visible(true);
	}
	do {++current_page;} while (!state.dictionary_entries["i" + current_page.tostring()]);
	if(state.dictionary_entries["i" + current_page.tostring()] == "END") {
		sector.Text.set_visible(false);
		sector.TextArray.set_visible(false);
		dict.set_visible(false);
		cover_back.set_visible(true);
		illustration.set_visible(false);
		play_sound("levels/narre2/sound/book.wav");
	} else {
		sector.Text.set_text(titles[current_page]);
		sector.TextArray.set_text(descriptions[current_page]);
		illustration.set_visible(false);
		illustration <- FloatingImage(illustrations[current_page]);
		illustration.set_anchor_point(ANCHOR_MIDDLE);
		illustration.set_pos(-290, 20);
		illustration.set_layer(102);
		illustration.set_visible(true);
		play_sound("levels/narre2/sound/page.wav");
	}
	
}

function previous_page() {
	if(!current_page) return;
	if(state.dictionary_entries["i" + current_page.tostring()] == "END") {
		cover_back.set_visible(false);
		dict.set_visible(true);
		sector.Text.set_visible(true);
		sector.TextArray.set_visible(true);
	}
	do {--current_page;} while(!state.dictionary_entries["i" + current_page.tostring()]);
	if(!current_page) {
		sector.Text.set_visible(false);
		sector.TextArray.set_visible(false);
		dict.set_visible(false);
		illustration.set_visible(false);
		cover_front.set_visible(true);
		play_sound("levels/narre2/sound/book.wav");
	} else {
		sector.Text.set_text(titles[current_page]);
		sector.TextArray.set_text(descriptions[current_page]);
		illustration.set_visible(false);
		illustration <- FloatingImage(illustrations[current_page]);
		illustration.set_anchor_point(ANCHOR_MIDDLE);
		illustration.set_pos(-290, 20);
		illustration.set_layer(102);
		illustration.set_visible(true);
		play_sound("levels/narre2/sound/page.wav");
	}
}

function close_dictionary() {
	sector.Text.set_visible(false);
	sector.TextArray.set_visible(false);
	cover_front.set_visible(false);
	cover_back.set_visible(false);
	dict.set_visible(false);
	illustration.set_visible(false);
}
