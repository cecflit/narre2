//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//return true if all secret areas have been found
function all_secrets() {
	return state.w6_ldeepplaces_s1 && state.w6_ldeepplaces_s2 && state.w5_lmntbreeze_s1 && state.w4_labandonlair_s1 && state.w3_llayered_s1 && state.w3_lthorny_s1 && state.w2_lspikyrifts_s2 && state.w2_lmoisttunnel_s2 && state.w2_lmoisttunnel_s3 && state.w2_lspikyrifts_s1 && state.w19_lfork_s1 && state.w17_lcomplex_s1 && state.w17_lcomplex_s2 && state.w17_lcomplex_s3 && state.w17_lcomplex_s4 && state.w15_lcloverflds_s1 && state.w14_lladtramp_s1 && state.w11_ldrought_s1 && state.w9_lbitter_s1 && state.w8_lnoveg_s1 && state.w7_lcrcl_s1 && state.w5_lconquer_s1 && state.w4_lll_s1 && state.w2_lforshrum_s1 && state.w1_liceland_s1 && state.w18_lvirmad_s1 && state.w16_lhothumid_s1 && state.w14_ldim_s1 && state.w13_lcalmflow_s1 && state.w12_ldefheat_s1 && state.w11_lcaczone_s1 && state.w10_lghostspot_s1 && state.w9_lfridgy_s1 && state.w7_lpurplecave_s1 && state.w6_ldeepcold_s1 && state.w3_lgloomy_s1 && state.w2_lsumwood_s2 && state.w2_lsumwood_s1 && state.w18_ldiginsane_s1 && state.w17_ltraps_s1 && state.w16_llandhot_s1 && state.w15_llandhay_s1 && state.w14_lstonezone_s1 && state.w13_lwetwater_s2 && state.w13_lwetwater_s1 && state.w12_lwarmcore_s1 && state.w10_lmuddyswamp_s1 && state.w8_lrocksolid_s2 && state.w8_lrocksolid_s1 && state.w5_lsnowypeaks_s1 && state.w4_ldarktunnel_s1 && state.w3_lexplore_s1 && state.w2_lheavyr_s1 && state.w1_lmassgla_s1 && state.w16_lhotspring_s1 && state.w15_lladderspike_s1 && state.w13_ltinyisle_s1 && state.w12_lllakes_s2 && state.w12_lllakes_s1 && state.w11_lsmalloasis_s1 && state.w10_loverwisp_s1 && state.w9_lisleice_s1 && state.w8_lundercave_s1 && state.w4_lburyruin_s1 && state.w2_lrunwoods_s1 && state.w1_lplainsice_s1 && state.w18_llaserrun_s1 && state.w17_lexplodunge_s1 && state.w15_lburningway_s1 && state.w14_lrockcrush_s1 && state.w13_lyetbridge_s1 && state.w12_lburntplat_s1 && state.w11_lquickpuddles_s1 && state.w10_lmuddive_s1 && state.w9_lfairfrost_s1 && state.w8_lexplorelake_s1 && state.w7_lrubyred_s1 && state.w6_lgetslippery_s1 && state.w4_lwaterfalls_s1 && state.w3_lentertomb_s1 && state.w2_lspring_s1 && state.w1_lpipej_s1 && state.w18_lpushers_s1 && state.w17_lmovings_s1 && state.w15_lgrplateau_s1 && state.w14_lladderjump_s1 && state.w10_lshowme_s1 && state.w1_lfrozenhills_s1 && state.w1_lfrozenhills_s2 && state.w1_ldownfrozencave_s1 && state.w1_ldownfrozencave_s2 && state.w1_ljustanothersnowyday_s1 && state.w1_ljustanothersnowyday_s2 && state.w1_licypath_s1 && state.w2_lwelcomeforest_s1 && state.w2_loverglade_s1 && state.w2_lmoisttunnel_s1 && state.w2_lmolehole_s1 && state.w3_lwelcomejungle_s1 && state.w3_lsneakycliff_s1 && state.w4_lhillderground_s1 && state.w4_lgenerictunnel_s1 && state.w5_lwelcomemountains_s1 && state.w5_lreallyhigh_s1 && state.w5_ldontvalley_s1 && state.w6_lfrozendepth_s1 && state.w6_lfrozendepth_s2 && state.w6_lspikes_s1 && state.w6_lfreezingcave_s1 && state.w7_lwelcomecrystal_s1 && state.w7_ldiamondssector_s1 && state.w8_llakevalley_s1 && state.w9_ltreeguards_s1 && state.w10_lmuddylakes_s1 && state.w10_lmuddylakes_s2 && state.w11_lwelcomedesert_s1 && state.w11_lsandcave_s1 && state.w11_lnoshade_s1 && state.w12_lwelcomevolcano_s1 && state.w12_lerupt_s1 && state.w12_lburnbridge_s1 && state.w13_lwelcomeriver_s1 && state.w13_lboatwrld_s1 && state.w13_lweird_s1 && state.w13_lspiky_s1 && state.w14_lwelcomenrwcave_s1 && state.w14_lceilcoll_s1 && state.w14_lzigzag_s1 && state.w14_lzigzag_s2 && state.w14_lzigzag_s3 && state.w14_lzigzag_s4 && state.w15_lwavy_s1 && state.w15_lplainflowers_s1 && state.w15_lstripy_s1 && state.w16_lwelcomegeysers_s1 && state.w17_lwelcomefort_s1 && state.w17_lspacedng_s1 && state.w17_lspacedng_s2 && state.w17_lflood_s1 && state.w17_llavapol_s1 && state.w18_lwelcomematr_s1 && state.w18_llasertraps_s1 && state.w18_llifts_s1 && state.w18_lboxbeam_s1 && state.w1_lssland_s1 && state.w1_lssland_s2 && !state.achievement_to_find_a_needle_in_a_haystack;
}

//grant the 'To Find a Needle in a Haystack' achievement if all secrets have been found
function check_secretareas() {
	if(all_secrets()){
		state.achievement_to_find_a_needle_in_a_haystack <- true;
		show_notification("levels/narre2/images/notifications/to_find_a_needle_in_a_haystack/achievement.png");	
	}
}

//check and potentially grant the 'Nested!' and/or 'To Find a Needle in a Haystack' achievements
function check_for_nesting() {
	if(state.achievement_to_find_a_needle_in_a_haystack && !state.achievement_nested) {
		show_notification("levels/narre2/images/notifications/nested/achievement.png");
		state.achievement_nested <- true;
	} else if(!state.achievement_to_find_a_needle_in_a_haystack && !state.achievement_nested) {
		if(!all_secrets()) {
			show_notification("levels/narre2/images/notifications/nested/achievement.png");
			state.achievement_nested <- true;
		} else {
			show_notification("levels/narre2/images/notifications/to_find_a_needle_in_a_haystack/achievement.png", "levels/narre2/images/notifications/nested/achievement.png");
			state.achievement_nested <- true;
			state.achievement_to_find_a_needle_in_a_haystack <- true;
		}
	} else if(!state.achievement_to_find_a_needle_in_a_haystack && state.achievement_nested) {
		if(all_secrets()) {
			show_notification("levels/narre2/images/notifications/to_find_a_needle_in_a_haystack/achievement.png");
			state.achievement_to_find_a_needle_in_a_haystack <- true;
		}
	}
}

//check whether all levels are finished
function checkalllevelsdone() {
	//Make sure all worlds are unlocked
	if(!("/levels/narre2/bonusland_pipe_world.stwm" in state.worlds) || !("/levels/narre2/bonusland_industrial_world.stwm" in state.worlds) || !("/levels/narre2/bonusland_crystal_world.stwm" in state.worlds) || !("/levels/narre2/bonusland_lava_cavern.stwm" in state.worlds)) return false;
	foreach(lvl in state.worlds["/levels/narre2/worldmap.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/forest.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/jungle.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/underground.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/mountains.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/icecave.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/crystalcave.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/rocks.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/icecanyon.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/swamp.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/desert.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/volcano.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/river.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/stonecave.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/meadows.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/geysers.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/fortress.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/matrix.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_mainland.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_industrial_world.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_lava_cavern.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_crystal_world.stwm"]["levels"]) if(!lvl.solved) return false;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_pipe_world.stwm"]["levels"]) if(!lvl.solved) return false;

	return true;
}

//check how many levels are not finished perfectly
function check_nonperfect() {
	if(!state.achievement_grand_traveller) return 400;
	ret <- 0;

	foreach(lvl in state.worlds["/levels/narre2/worldmap.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/forest.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/jungle.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/underground.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/mountains.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/icecave.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/crystalcave.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/rocks.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/icecanyon.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/swamp.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/desert.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/volcano.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/river.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/stonecave.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/meadows.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/geysers.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/fortress.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/matrix.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_mainland.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_industrial_world.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_lava_cavern.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_crystal_world.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	foreach(lvl in state.worlds["/levels/narre2/bonusland_pipe_world.stwm"]["levels"]) if(!lvl.perfect) ++ret;
	return ret;
}

//give the player the 'Overcharged' achievement
function give_ach_immor() {
	state.achievement_immortality_confirmed <- true;
	show_notification("levels/narre2/images/notifications/immortality/achievement.png");
}

//give the player the 'First Steps' achievement
function give_ach_fsteps() {
	if(!state.achievement_first_steps) {
		state.achievement_first_steps <- true;
		show_notification("levels/narre2/images/notifications/first_steps/achievement.png");
	}
}

//give the player the 'The Crossroads' achievement if earned
function check_and_give_crossroads() {
	if(!state.achievement_crossroads && state.pipeworld && state.industrialworld && state.lavacavern && state.crystalworld) {
		state.achievement_crossroads <- true;
		show_notification("levels/narre2/images/notifications/crossroads/achievement.png");
	}
}

//dead-lock the 'I Won't Die Anyway' achievement unless it's already unlocked
function lock_wontdie() {
	if(state.achievement_i_wont_die_anyway == 1) {
		state.achievement_i_wont_die_anyway <- 3;
		show_notification("levels/narre2/images/notifications/i_wont_die_anyway/achievement_lock.png", false, "levels/narre2/sound/deadlock.wav");
	}
}

//trigger an endsequence and check whether the player should get the 'The Grand Traveller' achievement, if so, grant it
function endseq() {
	sector.Tux.trigger_sequence("endsequence");
	if(!state.achievement_grand_traveller && checkalllevelsdone()) {
		state.achievement_grand_traveller <- true;
		show_notification("levels/narre2/images/notifications/the_grand_traveller/achievement.png");
	}
}

//trigger an endsequence and check whether the player should get the 'The Grand Traveller' or the 'Overcharged' achievement (or both)
function endseq_immor() {
	if(state.weather != 3 || state.achievement_immortality_confirmed) endseq();
	else if(state.achievement_grand_traveller || !checkalllevelsdone()) {
		sector.Tux.trigger_sequence("endsequence");
		state.achievement_immortality_confirmed <- true;
		show_notification("levels/narre2/images/notifications/immortality/achievement.png");
	} else {
		sector.Tux.trigger_sequence("endsequence");
		state.achievement_immortality_confirmed <- true;
		state.achievement_grand_traveller <- true;
		show_notification("levels/narre2/images/notifications/the_grand_traveller/achievement.png", "levels/narre2/images/notifications/immortality/achievement.png");
	}
}
