//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

function play_intro() {
	igloo <- FloatingImage("levels/narre2/images/cutscenes/intro/with.png");
	lone <- FloatingImage("levels/narre2/images/cutscenes/intro/without.png");
	pex <- FloatingImage("levels/narre2/images/cutscenes/intro/pex.png");
	pex_m <- FloatingImage("levels/narre2/images/cutscenes/intro/pex_matrix.png");
	igloo.set_anchor_point(ANCHOR_MIDDLE);
	lone.set_anchor_point(ANCHOR_MIDDLE);
	pex.set_anchor_point(ANCHOR_MIDDLE);
	pex_m.set_anchor_point(ANCHOR_MIDDLE);
	igloo.set_pos(0, 0);
	pex.set_pos(0, 0);
	pex_m.set_pos(0, 0);
	lone.set_pos(0, 0);

	sector.Effect.sixteen_to_nine(1);
	start_cutscene();
	if(state.voice) stop_music(6);

	if(state.voice) play_sound("levels/narre2/speech/intro.ogg");

	Text.set_text(_("[Mysterious voice] So, you came."));

	if(state.subtitles) Text.fade_in(1);

	wait(2.321);
	Text.set_text(_("[Mysterious voice] About time, I must say."));
	wait(1.879);
	Text.set_text(_("[Mysterious voice] And before you start,"));
	wait(1.49);
	Text.set_text(_("[Mysterious voice] you should know why"));
	wait(1.61);
	Text.set_text("");
	wait(0.9);
	Text.set_text(_("[Narre] Not long ago, Tux and Penny were having a nice time out with their neighbours,"));
	wait(4.259);
	Text.set_text(_("[Narre] out in the ice fields of Antarctica."));
	wait(2.541);
	Text.set_text(_("[Narre] When suddenly, the neighbours disappeared."));

	igloo.fade_in(0.5);

	wait(2.207);
	Text.set_text(_("[Narre] ZAP! Just like that."));

	lone.fade_in(0.2);

	wait(3.075);

	lone.fade_out(0.5);
	igloo.fade_out(0.01);

	Text.set_text(_("[Narre] Concerned, Tux and Penny decided it'd be best if they returned home."));
	wait(4.425);
	Text.set_text(_("[Narre] Alas, no home was safe."));
	wait(2.4);
	Text.set_text(_("[Narre] Not long after, Tux woke up in the middle of a starry night,"));
	wait(3.445);
	Text.set_text(_("[Narre] to a ZAP, not unlike the last one."));
	wait(3.055);
	Text.set_text(_("[Narre] And Penny was gone. Right in front of his eyes."));
	wait(4.2);
	Text.set_text(_("[Narre] Concerned for her, Tux started investigating."));
	wait(3.4);
	Text.set_text(_("[Narre] Soon he found out that many creatures were disappearing from the neighbourhood."));
	wait(3.96);
	Text.set_text(_("[Narre] And not just the neighbourhood, similar cases were reported all over Antarctica,"));
	wait(4.84);
	Text.set_text(_("[Narre] and possibly the entire world."));
	wait(2.2);
	Text.set_text(_("[Narre] Unable to find an explanation, Tux turned to the local old, all-knowing,"));
	wait(5.5);
	Text.set_text(_("[Narre] widely respected, but also somewhat crazy and smelly penguin, Pex."));
	wait(5.1);
	Text.set_text(_("[Narre] As Pex was getting old, his theories sounded more like conspiracies,"));
	wait(4.27);
	Text.set_text(_("[Narre] but even then, he was rarely wrong."));
	wait(2.73);

	pex.fade_in(0.5);

	Text.set_text(_("[Narre] When Tux went to see Pex, he was running circles around his igloo"));
	wait(4.5);
	Text.set_text(_("[Narre] and singing Christmas carols - in mid April."));
	wait(3.65);
	Text.set_text(_("[Narre] He, however, came up with a theory."));
	wait(3.35);
	Text.set_text(_("[Narre] It sounded crazy, but it was Pex who said it, so it was probably right."));
	wait(3.9);

	pex_m.fade_in(0.2);

	Text.set_text(_("[Narre] He talked about a machine, a powerful computer being built in the sea,"));
	wait(4.97);
	Text.set_text(_("[Narre] close to Nolok's massive fortress."));
	wait(2.83);

	pex_m.fade_out(0.5);
	pex.fade_out(0.01);

	Text.set_text(_("[Narre] Not the one on IcyIsland, but one twenty times the size,"));
	wait(3.9);
	Text.set_text(_("[Narre] built in the middle of the ocean, that Nolok inherited"));
	wait(3.6);
	Text.set_text(_("[Narre] from his father. This computer was allegedly carrying out Nolok's plan"));
	wait(5.05);
	Text.set_text(_("[Narre] to capture all life on Earth and put it into a virtual zoo"));
	wait(4.03);
	Text.set_text(_("[Narre] where he could play with everyone like in a video game."));
	wait(3.78);
	Text.set_text(_("[Narre] No one was safe. This was everyone's fate."));
	wait(3.76);
	Text.set_text(_("[Narre] Eventually, only Nolok would be left."));
	wait(2.68);
	Text.set_text(_("[Narre] This computer, which Pex called 'the Matrix', was supposed to be"));
	wait(5.2);
	Text.set_text(_("[Narre] almost impenetrable - protected by all sorts of crazy equipment."));
	wait(4.73);
	Text.set_text(_("[Narre] Tux has heard enough. He would go to this Matrix, disable it"));
	wait(4.42);
	Text.set_text(_("[Narre] once and for all, and defeat Nolok for the last time,"));
	wait(3.65);
	Text.set_text(_("[Narre] and this time, for good."));
	wait(3);
	Text.set_text(_("[Narre] He immediately packed up, said goodbye to his neighbourhood,"));
	wait(3.25);
	Text.set_text(_("[Narre] and left for his next adventure."));
	wait(2.75);
	Text.set_text(_("[Narre] In this quest, Tux will have to cross 16 islands,"));
	wait(3.76);
	Text.set_text(_("[Narre] Nolok's fortress and the Matrix itself."));
	wait(3.19);
	Text.set_text(_("[Narre] All but the Matrix are protected by Nolok's minions,"));
	wait(3.55);
	Text.set_text(_("[Narre] and the Matrix with something even more dangerous, if Pex was to be believed."));
	wait(4.7);
	Text.set_text(_("[Narre] But Tux wouldn't let any of that scare him away."));
	wait(3);
	Text.set_text(_("[Narre] Destiny of the entire world was at stake."));
	wait(3);
	Text.set_text("");
	wait(1.3);
	Text.set_text(_("[Mysterious voice] So you heard, Player. And you will help."));
	wait(3);
	Text.set_text(_("[Mysterious voice] Should you refuse, Tux ... will fail."));
	wait(3.16);
	Text.set_text(_("[Mysterious voice] If he fails, then you too,"));
	wait(2.14);
	Text.set_text(_("[Mysterious voice] will soon feel, the coming doom."));
	wait(2.2);
	
	if(state.subtitles) Text.fade_out(1);
	wait(1);
	
	state.played <- true;
	end_cutscene();
	sector.Effect.four_to_three(1);
	sector.Effect.fade_out(2);
	wait(2);

	state.jwintro <- true;
	restart();
}
