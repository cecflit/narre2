//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Trigger on-reload events
import("levels/narre2/scripts/reload_handler.nut");

//Night
if(state.daytime) night.fade(1, 1);
else night.fade(0, 1);
