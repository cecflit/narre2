//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Change daytime to according to the settings
function change_daytime() {
	if(!state.setting_daytime) {
		state.daytime <- !state.daytime;
	} else {
		state.daytime = state.setting_daytime - 1;
	}
}

//choose random weather (if set to cycle); 4/7 clear, 1/7 cloudy, 1/7 rain/snow, 1/7 thunderstorm
function change_weather() {
	if(state.setting_weather == -1) {
		state.weather <- random(6);
		if(state.weather > 3) state.weather <- 0;
	} else state.weather <- state.setting_weather;
}

//manual lightning strike
function lightning(day, swamp) {
	wait((random(8) + 2));			//wait 2-10 seconds
	WEATH_3.thunder();			//thunder
	wait(2);				//wait 2 seconds
	WEATH_3.lightning();			//strike
	if(state.visual_effects >= 2) settings.set_ambient_light(1, 1, 1);
						//set ambient light to full brightness

	//if non-electrifiable water is present in the level, strike it manually, else wait the time it would take
	if(state.scelectrify) waterstrike(swamp);
	else wait(0.35);
	if(state.visual_effects < 2) return;
	if(!day) {
		//set ambient light back to half brightness, if the player is in the tomb in 'Entering the Tomb', add a green tint
		if(!tombthunder) settings.set_ambient_light(0.5, 0.5, 0.5);
		else settings.set_ambient_light(0.4, 0.6, 0.4);
	} else {
		//set ambient light back to 3/4 brightness, or the appropriate lighting if the player is in the tomb
		if(!tombthunder) settings.set_ambient_light(0.75, 0.75, 0.75);
		else settings.set_ambient_light(0.4, 0.6, 0.4);
	}
}

//set the background and particles appropriate for the daytime and weather
function set_weather(swamp) {
	//Set the background to day + cloudy
	if(!state.daytime && state.weather > 0) {
		if(!swamp) noc.set_images("nightsky_bottom.png", "nightsky_bottom.png", "nightsky_bottom.png");
		else pozadi.set_images("nightsky_bottom.png", "nightsky_bottom.png", "nightsky_bottom.png");
	}

	//Set the background to night + cloudy
	if(state.daytime && state.weather > 0) {
		if(!swamp) noc.set_images("levels/narre2/images/background/night/nightsky_bottom-night.png", "levels/narre2/images/background/night/nightsky_bottom-night.png", "levels/narre2/images/background/night/nightsky_bottom-night.png");
		else pozadi.set_images("levels/narre2/images/background/night/nightsky_bottom-night.png", "levels/narre2/images/background/night/nightsky_bottom-night.png", "levels/narre2/images/background/night/nightsky_bottom-night.png");
	}

	//set the ambient light to day + cloudy
	if(!state.daytime && state.weather == 1) settings.set_ambient_light(0.9, 0.9, 0.9);

	//set the ambient light to day + rain/thunder
	if(!state.daytime && state.weather > 1) settings.set_ambient_light(0.75, 0.75, 0.75);

	//disable cloud and rain/snow particles if weather is clear
	if(!state.weather) {
		WEATH_1.set_enabled(false);
		WEATH_2.set_enabled(false);
	}

	//disable rain/snow particles if weather is cloudy
	if(state.weather == 1) {
		WEATH_2.set_enabled(false);
		if(!state.visual_effects) WEATH_1.set_enabled(false);
	}

	if(state.weather >= 2) {
		if(!state.visual_effects) {
			WEATH_1.set_enabled(false);
			WEATH_2.set_enabled(false);
		} else if(state.visual_effects < 3) {
			WEATH_2.set_enabled(false);
			WEATH_2_FAST.set_enabled(true);
		}
	}

	//randomise lightning
	while(state.weather == 3) {
		for(i <- 0; i < 3; ++i) lightning(!state.daytime, swamp);
		wait(12); //wait 12 seconds to ensure the player always has enough time to pass through swimming sections
	}
}

//manually electrify non-electrifiable water (over is a tilemap used in some levels where 2 water tilemaps are needed)
function waterstrike(over) {
	elewater.fade(1, 0);
	if(over == 1) elewaterover.fade(1, 0);
	wait(0.35);
	elewater.fade(0, 0);
	if(over == 1) elewaterover.fade(0, 0);
}
