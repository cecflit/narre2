//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Trigger on-reload events
import("levels/narre2/scripts/reload_handler.nut");

//Secret place
if(state.river_secret) Secret_Path.fade(1, 0);

//Night
if(state.daytime) night.fade(1, 1);
else night.fade(0, 1);

//Unlock the world
state.river <- true;
