//Narre's Multiworld
//Squirrel Scripting
//v0.4.9

//Import global variables
import("levels/narre2/scripts/globals.nut");

//Imports some miscellaneous functions
import("levels/narre2/scripts/misc.nut");

//Import weather and daytime handling functions
import("levels/narre2/scripts/weather.nut");

//Import functions that handle achievements
import("levels/narre2/scripts/achievements.nut");
